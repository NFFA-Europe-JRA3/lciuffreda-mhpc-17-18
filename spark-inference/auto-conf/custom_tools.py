from __future__ import print_function
import pandas as pd
import socket
import os
import subprocess
import time
import psutil
import numpy as np

##### set defaults
spark_home='/opt/spark/'
spark_home_conf=os.path.join(spark_home,'conf/')
config_file=os.path.join(os.getcwd(),'cluster.conf')
log_dir=os.path.join(os.getcwd(),'log/')
log_filename=os.path.join(log_dir,'config_log_'+time.strftime("%d-%m-%y-%H:%M:%S")+'.txt')
temp_dir=os.path.join(os.getcwd(),'temp/') # temporary files for communication: to be removed at the end of execution

def check_dir(directory):
    if not os.path.isdir(directory):
        os.makedirs(directory)

class LogHandle(object):
    def __init__(self,FileHandle=None):
        self.filehandle=FileHandle
        assert FileHandle!=None, 'ERROR: the log handle must be initialized with a proper file handle!'
    def write(self,msg,dtype='str'):
        if dtype=='df':
            msg.to_string(self.filehandle)
            self.filehandle.write('\n')
        elif dtype=='dict':
            self.filehandle.write(time.strftime("%H:%M:%S"))
            for key, value in msg.items():
                self.filehandle.write('%s:%s\n' % (key, value))
            self.filehandle.write('\n')            
        elif dtype=='str':
            self.filehandle.write(time.strftime("%H:%M:%S")+': '+msg+'\n')
    def set_title(self,msg):
        self.filehandle.write('------------------------------------------------------------------------------------------------------\n')
        self.filehandle.write(msg+'\n')
        self.filehandle.write('------------------------------------------------------------------------------------------------------\n')
    
def backup_etchosts():
    '''
    Check if is present a backup of /etc/hosts; if not it creates it.
    '''
    if not os.path.isfile('/etc/hosts.bkp'):
        CMD='cp /etc/hosts /etc/hosts.bkp'
        subprocess.call(CMD,shell=True)

def kill_java_procs():
    '''
    Kill every java job, possibly occupying ports needed by Spark.
    '''
    procname='java'
    for proc in psutil.process_iter():
        if proc.name()==procname:
            proc.kill()

def clean(x):
    return x.strip()

class Node(object):
    def __init__(self, hostname=None, role=None, ip=None):
        self.hostname = hostname
        self.role = role
        self.ip = ip
    def prop(self):
        print('Hostname:', self.hostname)
        print('IP:', self.ip)
        print('Role:', self.role)
    def start_spark_daemon(self,master=None):
        buffer_master=os.path.join(temp_dir,'buffer_master')
        buffer_master_url=os.path.join(temp_dir,'buffer_master_url')
        if self.role=='master':
            CMD=os.path.join(spark_home,'sbin/start-master.sh')+' > '+buffer_master
            subprocess.call(CMD,shell=True)
            with open(buffer_master,'r') as f:
                master_log=f.readline()
            master_log=master_log.split()[-1]
            print('master has logged in ', master_log)
            CMD='cat '+master_log+' > '+ buffer_master
            subprocess.check_output(CMD,shell=True)
            CMD='cat '+ buffer_master+ ' | grep "Starting Spark master at" | awk '+"'{print $10}' > " +buffer_master_url
            subprocess.check_output(CMD,shell=True)
        elif self.role=='slave':
            if wait_file(buffer_master_url):
                time.sleep(2)
                with open(buffer_master_url,'r') as f:
                    spark_master_url=f.readline()
                spark_master_url=spark_master_url.strip('\n')
                print('connecting to ', spark_master_url)    
                CMD=os.path.join(spark_home,'sbin/start-slave.sh') +' '+spark_master_url
                subprocess.call(CMD,shell=True)
            
def wait_file(filename, timeout=30, period=0.25):
  max_end=time.time()+timeout
  start=time.time()
  while time.time() < max_end:
    if os.path.exists(filename): return True
  return False

def get_role_config(_config_file):
    hosts=pd.read_table(_config_file,header=None,names=['hostname','role'])
    hosts['hostname']=hosts['hostname'].apply(clean)
    return hosts

def get_cluster(cluster_file):
    cluster=pd.read_table(cluster_file,header=0,sep='\t')
    return cluster

def get_master(cluster_df):
    return list(cluster_df.loc[cluster_df.role=='master'].hostname.values)[0]

def set_spark_conf(filename,conf_dict,sep):
    assert os.path.exists(os.path.join(spark_home_conf,filename+'.template')), 'ERROR: file %s is not a possible spark configuration file' % filename
    with open(os.path.join(spark_home_conf,filename),'w') as f:
        for val in conf_dict:
            f.write(val+sep+str(conf_dict[val])+'\n')
