#!/usr/bin/python
from __future__ import print_function
import pandas as pd
import socket
import os
import subprocess
import time
import custom_tools as tl
from optparse import OptionParser
import shutil

def main():

    ##### set defaults
    spark_home='/opt/spark/'
    spark_home_conf=os.path.join(spark_home,'conf/')
    config_file=os.path.join(os.getcwd(),'cluster.conf')
    log_dir=os.path.join(os.getcwd(),'log/')
    log_filename=os.path.join(log_dir,'config_log_'+time.strftime("%d-%m-%y-%H:%M:%S")+'.txt')
    temp_dir=os.path.join(os.getcwd(),'temp/') # temporary files for communication: to be removed at the end of execution

    ##### parsing args
    parser = OptionParser()
    parser.add_option("-C", \
                      "--config-file", \
                      dest="config_file", \
                      default=config_file, \
                      help="Configuration file defining the cluster (default: cluster.conf)", \
                      metavar="CONFIG-FILE")

    ##### set environment
    tl.check_dir(log_dir)
    tl.check_dir(temp_dir)
    tl.backup_etchosts()
    tl.kill_java_procs()

    print('Starting cluster configuration...')

    with open(log_filename,'w') as flog:

        log=tl.LogHandle(flog)
        log.set_title('LOG: configuring Spark cluster')
        log.write('Cluster is configured according to: '+config_file)
        
        ##### define node properties
        localnode=tl.Node(hostname=socket.gethostname(),ip=socket.gethostbyname(socket.gethostname()))
        log.write('Reading cluster configuration file...')
        hosts=tl.get_role_config(config_file)
        log.write('DONE')
        localnode.role=list(hosts.loc[hosts.hostname==localnode.hostname].role)[0]
        log.write('Node properties...')
        localnode.prop()

        buffer_file=os.path.join(temp_dir,'buffer')
        log.write('Recontructing cluster properties...')
        if localnode.role=='master':
            with open(buffer_file,'w') as f:
                f.write('hostname'+'\t'+'ip'+'\t'+'role\n')
                f.write(localnode.hostname+'\t'+str(localnode.ip)+'\t'+localnode.role+'\n')
        else:
            if tl.wait_file(buffer_file):
                time.sleep(2)
                with open(buffer_file,'a') as f:
                    f.write(localnode.hostname+'\t'+str(localnode.ip)+'\t'+localnode.role+'\n')
                    
        time.sleep(10) # this in not nice, must look for a different solution
        cluster_df=tl.get_cluster(buffer_file)
        log.write('Cluster properties recovered...')
        log.write(cluster_df,dtype='df')
        other_nodes=cluster_df.loc[cluster_df.hostname!=localnode.hostname]

        log.write('Configuring /etc/hosts...')
        with open('/etc/hosts', 'a') as f:
            for node in list(other_nodes.hostname):
                f.write(str(list(other_nodes.loc[other_nodes.hostname==node].ip.values)[0])+'\t'+str(node)+'\n')
        log.write('DONE')

        master_node=tl.get_master(cluster_df)
        log.write('Spark master set to...'+master_node)

        # setting spark conf files
        # 1] create spark-env.sh
        log.write('Setting Spark Conf files...')
        log.write('[1] spark-env.sh')
        spark_env_dict={'SPARK_MASTER_HOST': master_node, \
                        'SPARK_LOCAL_IP': localnode.ip}
        if localnode.role=='master':
            spark_env_dict['SPARK_WORKER_CORES']=0
        tl.set_spark_conf('spark-env.sh',spark_env_dict,'=')
        log.write(spark_env_dict,dtype='dict')
  
        # 2] create slaves
        log.write('[2] slaves')
        if localnode.role=='master':
            with open(os.path.join(spark_home_conf,'slaves'),'w') as f:
                for slave in other_nodes.hostname:
                    f.write(slave+'\n')
                    log.write(slave)
                    
        # 3] create spark-defaults.conf
        log.write('[3] spark-default.conf')
        spark_default_dict={'spark.master': 'spark://'+master_node+':7077', \
                            'spark.serializer': 'org.apache.spark.serializer.KryoSerializer'}
        tl.set_spark_conf('spark-defaults.conf',spark_default_dict,'\t')
        log.write(spark_default_dict,dtype='dict')

        localnode.start_spark_daemon(master_node)

        ##### remove temp
        if localnode.role!='master':
            time.sleep(2)
            shutil.rmtree(temp_dir,ignore_errors=True)

    print('Configuration complete. Check log:', log_filename)
    
if __name__ == '__main__':
    main()



    
