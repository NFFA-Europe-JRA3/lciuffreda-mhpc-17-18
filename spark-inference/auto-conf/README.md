Author: Luca Ciuffreda

# Configuring spark-docker cluster

This folder contains files to configure a Spark Standalone cluster: in order to do so it necessary to set up a Docker Swarm cluster.

## Set up Docker swarm cluster and overlay network

	(see http://blog.nigelpoulton.com/demystifying-docker-overlay-networking/)

	The swarm cluster must be initialized on a master node,

		docker swarm init --advertise-addr #MASTER_IP

	Once the cluster is initialized you can add other nodes running from them 

		docker swarm join --token ... --advertise-addr #WORKER_IP #MASTER_IP:2377

	where *token* is a string in the output of docker swarm init that defines the cluster. From the master you can now change roles of the nodes and add labels and tags if needed. Once the cluster is created 		you can create an overlay network: run from master

		docker network create -d overlay #NETWORK_NAME	

	This can be use when running a service.

## Spark cluster configuration

	The script *autoconfigure.py* sets up automatically the cluster given a simple input file, *cluster.conf*, that looks like this

		b22	master
		b24.hpc.c3e	slave
		b25.hpc.c3e	slave
		b26.hpc.c3e	slave

	The only role options are 'master'/'slave'. These roles don't need to be related to the swarm roles. (NOTE: here i assume to have 1 spark worker per host, if not changes need to be done). 
	The spark files configurations can be setted modifying the autoconfigure.py script where needed.

	The *autoconfigure.py* scritp must be run on every Docker node at the same time, since some communication between the nodes is necessary. 
	A log file will be saved in the log directory.

## Files

### autoconfigure.py

	Python script that configures the Spark cluster. To adjust the Spark files configurations, change this script accordingly.

### cluster.conf

	Configuration file of the cluster: a list of hostnames and roles ('master'/'slave').

### custom_tools.py

	Definition of funcitons use in *autoconfigure.py*.

### Dockerfile.spark_tf

	Docker file to create the Spark image.

### reset-hosts

	Bash script to reset the /etc/hosts file. Can be used if the cluster configuration needs to be repeated, if any error occured. 

## Remarks

	(i) if resetting the cluster, kill any *java* process on any machine; this processes might be Spark daeomns of previouos configurations.



