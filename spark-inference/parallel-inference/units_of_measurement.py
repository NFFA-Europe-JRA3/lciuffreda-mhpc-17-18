# -*- coding: utf-8 -*-

'''
Created on 24/giu/2014

@author: Stefano
'''

class NoUnitOfMeasurementFound(Exception):
    pass

class unit_of_measurement(object):

    unit_list = []
    
    def __init__(self, name, main_symbol, all_symbols = []):
        self.__name = name
        self.__symbol = main_symbol
        all_symbols = all_symbols[:]
        if main_symbol not in all_symbols:
            all_symbols.append(main_symbol)
        self.__symbols_list = all_symbols
        unit_of_measurement.unit_list.append(self)
        
    def get_name(self):
        return self.__name
    
    def get_symbol(self):
        return self.__symbol
    
    def __repr__(self):
        return self.__symbol
    
    def get_all_symbols(self):
        return self.__symbols_list
    

class primitive_unit(unit_of_measurement):
    def get_primitive(self):
        return self
    def is_primitive(self):
        return True
    def get_unit_factor(self):
        return 1.00
    

class secondary_unit(unit_of_measurement):
    def __init__(self, name, main_symbol, primitive, times_primitive, all_symbols=[]):
        super(secondary_unit, self).__init__(name, main_symbol, all_symbols[:])
        self.__primitive = primitive
        self.__tp = times_primitive
        
    def get_primitive(self):
        return self.__primitive
    
    def get_unit_factor(self):
        return self.__tp


def create_k_secondary(um):
    #print(um.get_all_symbols())
    return secondary_unit('kilo'+um.get_name().lower(), 'k'+um.get_symbol(), um, 1000.00)

def create_h_secondary(um):
    #print(um.get_all_symbols())
    return secondary_unit('hecto'+um.get_name().lower(), 'h'+um.get_symbol(), um, 100.00)

def create_c_secondary(um):
    #print(um.get_all_symbols())
    return secondary_unit('centi'+um.get_name().lower(), 'c'+um.get_symbol(), um, 0.01)

def create_m_secondary(um):
    return secondary_unit('milli'+um.get_name().lower(), 'm'+um.get_symbol(), um, 0.001)

def create_mu_secondary(um):
    return secondary_unit('micro'+um.get_name().lower(), 'ÃÂµ'+um.get_symbol(), um, 0.000001)

def create_n_secondary(um):
    return secondary_unit('nano'+um.get_name().lower(), 'n'+um.get_symbol(), um, 0.000000001)


metre = primitive_unit('metre', 'm')
kilometre = create_k_secondary(metre)
hectometre = create_h_secondary(metre)
centimetre = create_c_secondary(metre)
millimetre = create_m_secondary(metre)
micrometre = create_mu_secondary(metre)
nanometre = create_n_secondary(metre)

volt = primitive_unit('Volt', 'V')
kilovolt = create_k_secondary(volt)

ampere = primitive_unit('Ampere', 'A', ['A', 'amp'])
milliampere = create_m_secondary(ampere)
microampere = create_mu_secondary(ampere)
nanoampere = create_n_secondary(ampere)

bar = primitive_unit('bar','bar')
mbar = create_m_secondary(bar)


second = primitive_unit('second', 's', ['s', 'sec'])
minute = secondary_unit('minute', 'min', second, 60.0)
hours = secondary_unit('hours', 'h', second, 3600.0, ['h', 'Hours'])

def read_unit_number(raw_string):
    raw_words = raw_string.split()
    if len(raw_words) != 2:
        raise NoUnitOfMeasurementFound('There should be exactly two words in the string!')
    try:
        number = float(raw_words[0])
    except:
        raise NoUnitOfMeasurementFound("Can't cast the first word to a float!")
    right_uom = None
    for uom in unit_of_measurement.unit_list:
        if raw_words[1] in uom.get_all_symbols():
            right_uom = uom
            break
    else:
        raise NoUnitOfMeasurementFound("Unknown unit of measurement!")
    measure = number * right_uom.get_unit_factor()
    return [number, right_uom, measure, right_uom.get_primitive() ]
