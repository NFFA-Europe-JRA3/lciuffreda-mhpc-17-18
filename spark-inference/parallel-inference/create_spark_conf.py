'''
Author: Luca Ciuffreda

Creates a properties file for spark jobs to be added with flag --properties-file. Originally is used to set the variable spark.task.cpus, but it useful to set any spark variable for which there is no flag.

'''

from __future__ import print_function
import numpy as np
import os
import sys
import time
import pandas as pd
from optparse import OptionParser

parser = OptionParser()
parser.add_option("-C", "--conf", dest="conf", help="define cluster configuration as a index in params.conf file", metavar="CONF")
parser.add_option("-N", "--nodes", dest="nodes",type="int",help="number of hosts in the cluster (i.e. number of physical machines)", metavar="NODES")
(options, args) = parser.parse_args()

if len(args)!=0:
    print('ERROR: the number of arguments is wrong. Type %s -h to see usage.' % sys.argv[0])
    exit(1)
    
opts_dict=vars(options)
configuration=opts_dict['conf']
num_nodes=opts_dict['nodes']

# read conf inputs from params.conf
data_conf=pd.read_table('params.conf',header=0)

ex_node=data_conf.iloc[int(configuration)].EXEC_NODE
cores_ex=data_conf.iloc[int(configuration)].CORES_EX
mem_ex=data_conf.iloc[int(configuration)].MEM_EX

params={'spark.executor.memory':str(mem_ex)+'G','spark.executor.instances':ex_node*num_nodes,'spark.executor.cores':cores_ex,'spark.task.cpus':cores_ex}

with open('spark-add.conf', 'w') as f:
    for prop in params.keys():
        f.write(str(prop)+'\t'+str(params[prop])+'\n')
