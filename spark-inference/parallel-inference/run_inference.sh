#!/bin/bash

FILE=params.conf
# file defined as
# EXEC_NODE | CORES_EX | MEM_EX
#    ...        ...       ...
# for each configuration
export SPARK_HOME=/opt/spark/

DATA=$1
DATA_DIR=/data/local/nffa-test

cp SEM2hdf5.py /usr/local/lib/python2.7/dist_packages
cp units_of_measurement.py /usr/local/lib/python2.7/dist_packages

echo "Working on dataset... " ${DATA}

# skip first line
CNT=0
CONF=0
while read CMD; do
    if [ $CNT -eq 0 ]; then
	CNT=$((CNT+1))
    else
	# create properties file
	python create_spark_conf.py --conf $CONF --nodes 3
	cat ${SPARK_HOME}/conf/spark-defaults.conf >> spark-add.conf
	# clean folder
	STARTTIME=$(date +%s)
	spark-submit --verbose --properties-file spark-add.conf measure_inference.py \
		     --images-folder ${DATA_DIR}/$DATA \
		     --model-folder ${DATA_DIR}/output \
		     --convert-tif true
	ENDTIME=$(date +%s)
	echo "Total elapsed time:" $((ENDTIME-STARTTIME))
	echo "Used conf..."
    	cat spark-add.conf
	rm spark-add.conf
	CONF=$((CONF+1))
	fi
done < "$FILE"
