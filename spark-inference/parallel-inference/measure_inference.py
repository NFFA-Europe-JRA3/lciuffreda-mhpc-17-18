"""
Code to process tif images through image recognition in parallel on a Spark cluster.

Created by Rossella
Modified by Luca 

spark-submit *instert spark flags here* measure_inference.py \
                                        --images-folder [-I] /path/to/image/dir
                                        --model-folder [-M] /path/to/model
                                        --convert-tif  [True/False] [default=False]
                                        

"""

from __future__ import print_function
import numpy as np
import tensorflow as tf
import os
from tensorflow.python.platform import gfile
from PIL import Image
import sys
import time
import socket
from pyspark import SparkContext
from pyspark.sql import SparkSession
from optparse import OptionParser
from multiprocessing.pool import ThreadPool as Pool
import itertools
from functools import partial
import inspect
import json

# initialize spark context                                                                          
sc = SparkContext()
sc.addPyFile(os.path.join(os.getcwd(),'SEM2hdf5.py'))
sc.addPyFile(os.path.join(os.getcwd(),'units_of_measurement.py'))
import SEM2hdf5

# define task class
class classification_task(object):
    def __init__(self,model_name=None,labels=None,model_dir=None,images_dir=None,nex=1,nimages=0,parallelism=1,top_classes=5,qcompr=95):
        self.model_name=model_name          # name of the model
        self.labels=labels                  # labels of the classification
        self.model_dir=model_dir            # folder where the output.pb file of the model is saved
        self.images_dir=images_dir          # folder of images to be classified
        self.nex=nex                        # number of spark executors (remark: in original code nex=ncpus, because ncpus/nex is fixed to one. here we allow for different configs, seespark.executor.instances )
        self.parallelism=parallelism        # number of task that can be run in parallel by a spark ex (see spark.task.cpus)
        self.top_classes=top_classes        # number of top classes
        self.qcompr=qcompr                  # quality of the tif->jpg compression
        self.nimages=nimages
        if self.images_dir!=None:
            self.nimages=len([os.path.join(root, name) for root, dirs, files in os.walk(self.images_dir) for name in files if name.endswith(".tif")])
    def to_dict(self):
        attr=inspect.getmembers(self, lambda a:not(inspect.isroutine(a)))
        attr_list=[a for a in attr if not(a[0].startswith('__') and a[0].endswith('__'))]
        out_dict={}
        for key, val in attr_list:
            out_dict[key]=val
        return out_dict
    
# read inputs: this are saved in a classification_task global istance
parser = OptionParser()
parser.add_option("-I", "--images-folder", \
                  dest="images-folder", \
                  help="Defines the folder where the images are stored", \
                  metavar="IMAGES-FOLDER")
parser.add_option("-M", "--model-folder", \
                  dest="model-folder", \
                  help="Defines the folder where the model is stored", \
                  metavar="MODEL-FOLDER")
parser.add_option("--convert-tif", \
                  choices=['true','false'], \
                  default='false', \
                  dest="conval", \
                  help="If true converts every .tif in IMAGES-FOLDER to .jpg, else the .jpg files are expected to be already present: check this information before starting a session.", \
                  metavar="CONVERT")

(options, args) = parser.parse_args()

print(args)
assert len(args)==0, 'ERROR: the number of arguments is wrong. Type %s -h to see usage.' %  sys.argv[0]

opts_dict=vars(options)
missing_args=[]
for k in opts_dict.keys():
    if opts_dict[k]==None:
        missing_args.append(k)
assert len(missing_args)==0, 'ERROR: the following arguments are missing : ' + str(missing_args)

task=classification_task(model_name='Inception-v3',model_dir=opts_dict['model-folder'],images_dir=opts_dict['images-folder'])

def split_list(images_list):
    """Given a list of path to images, split it in `task.nex` sub-lists."""
    batches = [[] for _ in range(task.nex)]
    for img_idx, img_path in enumerate(images_list):
        batches[img_idx % task.nex].append(img_path)
    return batches

def tif2jpg(image):
    if os.path.splitext(image)[1].lower() == ".tif":
        outfile = os.path.splitext(image)[0] + ".jpg"
        try:
            # The default buffer size on linux is usually 8192 (it depends on
            # glibc).
            with open(image, "rb+") as im_fd:
                im = Image.open(im_fd).convert('RGB')
                im.save(outfile, "JPEG", quality=task.qcompr)
            return outfile
        except IOError as err:
            print(err)
            raise

def tif2jpg_batch(batch):
    if task.parallelism==1:
        jpg_list = []
        for image in batch:
            try:
                jpg_list.append(tif2jpg(image))
            except IOError as err:
                # There have been errors trying to convert the image to jpg, so we skip
                # the current image from the list of images to classify.
                pass
    else:
        # introduced to have multi-core task execution by spark executors. hopefully this can hel us understand the balance between n_executors/n_cores
        pool=Pool(processes=task.parallelism)
        jpg_list=pool.map(tif2jpg,batch)
        pool.close()
        pool.join()
    return jpg_list

# Output of the function are pairs (path of the image, list of top k results)
def run_image(sess, image):
    """ 
    Takes image and uses the trained neural network to infer the top task.top_classes categories.

    """
    if not gfile.Exists(image):
        tf.logging.fatal('File does not exist %s', image)
    image_data = gfile.FastGFile(image, 'rb').read()
    softmax_tensor = sess.graph.get_tensor_by_name('final_result:0')
    predictions = sess.run(
        softmax_tensor, {'DecodeJpeg/contents:0': image_data})
    predictions = np.squeeze(predictions)
    top_k = predictions.argsort()[-task.top_classes:][::-1]
    metadata = {}
    for i, label in enumerate(top_k):
        human_string=task.labels[label].strip('\n')
        score = predictions[label]
        metadata['label{}'.format(i)] = human_string
        metadata['label{}_rate'.format(i)] = score
    return (image, metadata)

# converting a batch of images from tif to hdf5
# Input is a tuple (jpg path, {labels, rate})
def tif2hdf5_aux(single_elem):
    image_path=single_elem[0]
    metadata=single_elem[1]
    tif_path = os.path.splitext(image_path)[0] + ".tif"
    metadata["created by host"] = socket.gethostname()
    try:
        SEM2hdf5.image2hdf5(tif_path, metadata)
        image_hdf5=os.path.splitext(image_path)[0] + ".hdf5"
    except UnicodeEncodeError as err:
        print("Unable to convert '{0}' to hdf5 due to non-ascii characters into its medadata.\n".format(tif_path), file=sys.stderr)
    except NotImplementedError as err:
        print("Unable to convert '{0}' to hdf5 because the image has an unrecognized color mode (error: {0}).\n".format(tif_path, err),file=sys.stderr)
    return image_hdf5

def tif2hdf5_batch(batch):
    if task.parallelism==1:
        list_hdf5 = []
        for elem in batch:
            list_hdf5.append(tif2hdf5_aux(elem))
        return list_hdf5
    else:
        # introduced to have multi-core task execution by spark executors. hopefully this can hel us understand the balance between n_executors/n_cores
        pool=Pool(processes=task.parallelism)
        list_hdf5=pool.map(tif2hdf5_aux,batch)#,chunksize=len(batch)/task.parallelism)
        pool.close()
        pool.join()
        return list_hdf5

# this works only for python2 (see: https://stackoverflow.com/questions/5442910/python-multiprocessing-pool-map-for-multiple-arguments#5443941)
def run_image_aux(sess_image):
    return run_image(*sess_image)
     
def main():

    if sc._conf.get('spark.executor.instances') is not None: 
        task.nex=int(sc._conf.get('spark.executor.instances'))
    else:
        task.nex=int(sc._conf.get('spark.dynamicAllocation.initialExecutors'))
    task.parallelism=int(sc._conf.get('spark.task.cpus'))

    if opts_dict['conval']=='false':
        convert=False
    elif opts_dict['conval']=='true':
        convert=True
        
    # sparkmeasure (see https://github.com/LucaCanali/sparkMeasure)
    from sparkmeasure import StageMetrics
    spark=SparkSession \
        .builder \
        .getOrCreate()
    stagemetrics = StageMetrics(spark)
    stagemetrics.begin()

    model_path = os.path.join(task.model_dir, 'output_graph.pb')
    with gfile.FastGFile(model_path, 'rb') as f:
        model_data = f.read()
    fname = os.path.join(task.model_dir, 'output_labels.txt')
    with open(fname) as f:
        task.labels=f.readlines()

    model_data_bc = sc.broadcast(model_data)
    content_bc = sc.broadcast(task.labels)
    
    def run_image_batch(batch):
        with tf.Graph().as_default() as g:
            graph_def = tf.GraphDef()
            graph_def.ParseFromString(model_data_bc.value)
            tf.import_graph_def(graph_def, name='')

            # Make TF use a single thread on each CPU (better performances on Lustre) // code here modified by Luca to test it
            # with intra_op_parallelism_threads=1
            # Force the use of CPUs with device_count GPU==0:
            # https://stackoverflow.com/a/43806059/5810105
            # Force TF use ONE core: https://stackoverflow.com/a/41819430/5810105
            conf = tf.ConfigProto(intra_op_parallelism_threads=1,
                                  inter_op_parallelism_threads=task.parallelism,
                                  device_count={'GPU': 0})
                
            with tf.Session(config=conf) as sess:
                if task.parallelism==1:
                    res = [run_image(sess,image) for image in batch]
                else:
                    pool=Pool(processes=task.parallelism)
                    aux=sess
                    run_image_aux = partial(run_image, sess)
                    res=pool.map(run_image_aux,batch)#,chunksize=len(batch)/task.parallelism)
                    pool.close()
                    pool.join()
                return [tup for tup in res]
            
    # look for .tif and create a list of paths
    tif_list=[os.path.join(root, name) for root, dirs, files in os.walk(task.images_dir) for name in files if name.endswith(".tif")]
    #if fraction<1.0:
    #    tif_list=tif_list[0:int(len(tif_list)*fraction)]    
    tif_list_batched = split_list(tif_list)
    
    if not convert:
        jpg_list=[os.path.join(root, name) for root, dirs, files in os.walk(task.images_dir) for name in files if name.endswith(".jpg")]
        #if fraction<1.0:
        #    jpg_list=jpg_list[0:int(len(jpg_list)*fraction)]    
        jpg_list_batched = split_list(jpg_list)

    print("There are %d images grouped in %d batches, one for each of the %d Spark executors available" %
          (len(tif_list), len(tif_list_batched), task.nex))
    print("The number of images after the split in batches is %d" %
          sum([len(l) for l in tif_list_batched]))
    print("length of each batch = ", [len(l) for l in tif_list_batched])
    print("Executors are %d" % task.nex)

    assert len(tif_list_batched) == task.nex

    if convert:
        tif_RDD = sc.parallelize(tif_list_batched, numSlices=len(tif_list_batched))
        jpg_RDD = tif_RDD.map(tif2jpg_batch)
        labelled_images = jpg_RDD.map(run_image_batch).persist()
        hdf5_RDD = labelled_images.flatMap(tif2hdf5_batch).collect()
    elif not convert:
        jpg_RDD=sc.parallelize(jpg_list_batched, numSlices=len(jpg_list_batched))
        labelled_images = jpg_RDD.map(run_image_batch).persist()
        hdf5_RDD = labelled_images.flatMap(tif2hdf5_batch).collect()
            
    stagemetrics.end()
    stagemetrics.print_report()

    tag=time.strftime("%d-%m-%y-%H:%M:%S")
  
    aggregatedDF = stagemetrics.aggregate_stagemetrics_DF("PerfStageMetrics")
    stagemetrics.save_data(aggregatedDF, 'out/aggr_stagemetrics_'+tag)

    print(task.to_dict())

    with open('out/aggr_stagemetrics_'+tag+'/conf-report.txt','w') as f:
        for key, value in task.to_dict().items():
            f.write('%s:%s\n' % (key, value))                       
                           
if __name__ == "__main__":
    main()
