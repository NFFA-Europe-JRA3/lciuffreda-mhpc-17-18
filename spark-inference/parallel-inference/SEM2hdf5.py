# -*- coding: utf-8 -*-

'''
Created on Apr 3, 2014

@author: stefano
'''
import numpy
from PIL import Image  # install with "pip install Pillow"
import h5py            # install with "pip install h5py"
import os, io
import sys
import time
from units_of_measurement import read_unit_number, NoUnitOfMeasurementFound
import json

class NoMicroscopeMetadata(Exception):
    pass

def key_dictionary(key=None, string_format = False):
    ''' This function returns a simple dictionary that links a number
    of a TIFF metadata field with a description for that field'''
    # I seguenti valori sono stati tratti dai file:
    # http://www.digitizationguidelines.gov/guidelines/TIFF_Metadata_Final.pdf
    # http://www.digitalpreservation.gov/formats/content/tiff_tags.shtml
    dict = {254 : numpy.string_('TIFF_NewSubfileType'),
            256 : numpy.string_('TIFF_Image_Width'),
            257 : numpy.string_('TIFF_Image_Height'),
            258 : numpy.string_('TIFF_Bits_Per_Sample'),
            259 : numpy.string_('TIFF_Compression'),
            262 : numpy.string_('TIFF_Photometric_Interpretation'),
            270 : numpy.string_('TIFF_Image_Description'),
            273 : numpy.string_('TIFF_Strip_Offsets'),
            277 : numpy.string_('TIFF_Samples_Per_Pixel'),
            278 : numpy.string_('TIFF_Rows_Per_Strip'),
            279 : numpy.string_('TIFF_Strip_Byte_Counts'),
            282 : numpy.string_('TIFF_X_Resolution'),
            283 : numpy.string_('TIFF_Y_Resolution'),
            284 : numpy.string_('TIFF_Planar_Configuration'),
            296 : numpy.string_('TIFF_Resolution_Unit'),
            320 : numpy.string_('TIFF_Color_Map')
           }
    if key == None:
        return dict
    else:
        if string_format:
            return dict[key].decode('UTF-8')[5:].replace('_',' ')
        else:
            return dict[key]

def metadata_dictionary():
    ''' Questa funzione restituisce una semplice lista dei metadata che
    Regina ci ha chiesto di salvare dalle immagini del SEM '''
    return {'DP_APERTURE':'Aperture No.',
            'DP_STAGE_INIT':'Stage Initialised',
            'DP_DETECTOR_CHANNEL':'Signal A',
            'DP_IMAGE_STORE':'Store resolution',
            'DP_TILTED':'Stage Tilted',
            'DP_TILT_CORRECTION':'Tilt Corrn.',
            'DP_SCM_RANGE':'SCM range',
            'DP_NOISE_REDUCTION':'Noise Reduction',
            'AP_C3':'C3 Lens I',
            'AP_WD':'WD',
            'AP_MAG':'Mag',
            'AP_BRIGHTNESS':'Brightness',
            'AP_CONTRAST':'Contrast',
            'AP_MANUALKV':'EHT Target',
            'AP_STIG_X':'Stigmation X',
            'AP_STIG_Y':'Stigmation Y',
            'AP_STAGE_GOTO_Z':'Stage goto Z',
            'AP_STAGE_GOTO_Y':'Stage goto Y',
            'AP_STAGE_GOTO_X':'Stage goto X',
            'AP_PIXEL_SIZE':'Pixel Size',
            'AP_COLUMN_VAC':'Gun Vacuum',
            'AP_SYSTEM_VAC':'System Vacuum',
            'AP_STAGE_LOW_Z':'Stage low Z',
            'AP_STAGE_LOW_Y':'Stage low Y',
            'AP_STAGE_LOW_X':'Stage low X',
            'AP_STAGE_HIGH_Z':'Stage high Z',
            'AP_STAGE_HIGH_Y':'Stage high Y',
            'AP_STAGE_HIGH_X':'Stage high X',
            'AP_PHOTO_NUMBER':'Photo No.',
          # I seguenti metadata non riguardano la misura in se ma lo stato
          # dello strumento
            'AP_BEAM_TIME':'Beam Time',
            'AP_EXTCURRENT':'Extractor I',
            'AP_ACTUALCURRENT':'Fil I',
            'AP_ACTUALKV':'EHT',
          # I seguenti metadata riguardano invece il software che la ha eseguita
            'SV_VERSION':'Version',
            'SV_SERIAL_NUMBER':'Serial No.',
            'SV_USER_TEXT':'User Text',
            'SV_OPERATOR':'Operator',
            'SV_USER_NAME':'User Name',
            'SV_FILE_NAME':'File Name',
            'SV_IMAGE_PATH':'Images'
             }


def open_image(file_path):
    imm = Image.open(file_path)
    metadata=imm.tag
    return imm, metadata

def get_palette(imm):
    palette = imm.getpalette()
    palette_list = []
    for i in range(0, len(palette),3):
        palette_list.append(palette[i:i+3])
    palette_array = numpy.array(palette_list)
    palette_array = palette_array.astype(numpy.uint8, copy=False)
    return palette_array

def get_image_data(imm):
    return numpy.array(imm)

def _extract_instrument_metadata(text, string_format = False):
    output = {}
    metadata_stream = io.StringIO(text)
    meta_dict = metadata_dictionary()
    for line in metadata_stream:
        title = line.rstrip('\n').strip()
        if title in meta_dict.keys():
            metadata = metadata_stream.readline()
            if not '=' in metadata:
                raise NotImplementedError('ERROR: Under the section '
                                          'named  "' + title +
                                          '" an unknown field has been found:\n' +
                                          metadata)
            metadata_name = metadata.split('=')[0].strip()
            if meta_dict[title]==metadata_name:
                value = metadata.split('=')[1].rstrip('\n').strip()
                if string_format:
                    output[metadata_name] = value
                else:
                    value = value.replace(u'µ','micro')
                    output[numpy.string_(metadata_name)]=numpy.string_(value)
    return output

def metadata_convert_hdf5(metadata):
    """ This function try to convert a metadata from a TIFF image
    in something that could be managed by hdf5. For example, a list
    of UTF-8 string become an array of ASCII string, an integer remain
    an integer, a float remain a float."""

    # Guardo se l'oggetto e` una stringa e se si lo converto in ascii, in altro
    # caso guardo se e` iterabile: se si restituisco un array con i valori
    # convertiti, altrimenti lascio l'oggetto cosi` com'e`
    if isinstance(metadata, str) or isinstance(metadata, bytes) or isinstance(metadata, unicode):
        return numpy.string_(metadata.encode('ascii', errors='xmlcharrefreplace'))
    else:
        try:
            return numpy.array([metadata_convert_hdf5(i) for i in metadata])
        except TypeError:
            return metadata

def metadata_convert_json(metadata):
    """ This function try to convert a metadata from a TIFF image
    in something that could be stored in a JSON file."""

    # Guardo se l'oggetto e` una stringa e se si lo converto in ascii, in altro
    # caso guardo se e` iterabile: se si restituisco un array con i valori
    # convertiti, altrimenti lascio l'oggetto cosi` com'e`
    if isinstance(metadata, str) or isinstance(metadata, bytes):
        return str(metadata)
    else:
        try:
            output = [metadata_convert_json(i) for i in metadata]
        except TypeError:
            return metadata
        if len(output) == 1:
            return output[0]
        else:
            return output


def __image2hdf5_palette(imm, metadata, imm_name, f):
    imm_data = get_image_data(imm)
    imm_palette = get_palette(imm)

    hdf5_imm = f.create_dataset(imm_name, data=imm_data, chunks=True, compression="gzip", compression_opts=9)
    hdf5_palette = f.create_dataset(imm_name + '_palette', data=imm_palette)
    f.flush()

    hdf5_imm.attrs['CLASS'] = numpy.array([numpy.string_('IMAGE')], dtype='|S6')
    hdf5_imm.attrs['PALETTE'] = hdf5_palette.ref
    hdf5_imm.attrs['IMAGE_SUBCLASS'] = numpy.array([numpy.string_('IMAGE_INDEXED')], dtype='|S13')
    hdf5_imm.attrs['IMAGE_VERSION'] = numpy.array([numpy.string_('1.2')], dtype='|S4')


    hdf5_palette.attrs['CLASS'] = numpy.array([numpy.string_('PALETTE')], dtype='|S8')
    hdf5_palette.attrs['PAL_COLORMODEL'] = numpy.array([numpy.string_('RGB')], dtype='|S4')
    hdf5_imm.attrs['IMAGE_VERSION'] = numpy.array([numpy.string_('STANDARD8')], dtype='|S10')
    hdf5_imm.attrs['IMAGE_VERSION'] = numpy.array([numpy.string_('1.2')], dtype='|S4')

    f.flush()

def __image2hdf5_RGB(imm, metadata, imm_name, f):
    imm_data = get_image_data(imm)

    hdf5_imm = f.create_dataset(imm_name, data=imm_data, chunks=True, compression="gzip", compression_opts=9)
    f.flush()

    hdf5_imm.attrs['CLASS'] = numpy.array([numpy.string_('IMAGE')], dtype='|S6')
    hdf5_imm.attrs['IMAGE_SUBCLASS'] = numpy.array([numpy.string_('IMAGE_TRUECOLOR')], dtype='|S16')
    hdf5_imm.attrs['IMAGE_MINMAXRANGE'] = numpy.array([0,255]).astype(numpy.uint8, copy=False)
    hdf5_imm.attrs['INTERLACE_MODE'] = numpy.array([numpy.string_('INTERLACE_PIXEL')], dtype='|S16')
    hdf5_imm.attrs['IMAGE_VERSION'] = numpy.array([numpy.string_('1.2')], dtype='|S4')

    f.flush()

def __image2hdf5_grey(imm, metadata, imm_name, f):
    imm_data = get_image_data(imm)

    hdf5_imm = f.create_dataset(imm_name, data=imm_data, chunks=True, compression="gzip", compression_opts=9)
    f.flush()

    hdf5_imm.attrs['CLASS'] = numpy.array([numpy.string_('IMAGE')], dtype='|S6')
    hdf5_imm.attrs['IMAGE_SUBCLASS'] = numpy.array([numpy.string_('IMAGE_GRAYSCALE')], dtype='|S16')
    hdf5_imm.attrs['IMAGE_MINMAXRANGE'] = numpy.array([0,255]).astype(numpy.uint8, copy=False)
    hdf5_imm.attrs['IMAGE_VERSION'] = numpy.array([numpy.string_('1.2')], dtype='|S4')
    pass


def image2hdf5(image_path, additional_metadata = None):
    imm, metadata = open_image(image_path)
    imm_name = os.path.splitext(os.path.basename(image_path))[0]
    hdf5_path = os.path.splitext(image_path)[0] + ".hdf5"

    if imm.mode != 'P' and imm.mode!='RGB' and imm.mode!='L':
        raise NotImplementedError('Only image color modes supported are:\n- Grey scale\n- RGB\n- Color palette')

    f = h5py.File(hdf5_path, 'w')

    if imm.mode == 'P':
        __image2hdf5_palette(imm, metadata, imm_name, f)

    if imm.mode == 'RGB':
        __image2hdf5_RGB(imm, metadata, imm_name, f)

    if imm.mode == 'L':
        __image2hdf5_grey(imm, metadata, imm_name, f)

    # Identifico ora la nuova tabella appena creata
    hdf5_imm = f[imm_name]

    # Costruiamo ora una lista di tutte le chiavi che identificano i metadata
    # Queste chiavi non sono nient'altro che il numero del campo che occupano
    # nella tiff.
    key_list = sorted(metadata)


    for key in key_list:
        key_dict = key_dictionary()
        # Il metadata 34118 e` il metadata dove sono contenuti tutti i dati
        # del SEM
        if key == 34118:
            meta_content = metadata[key]
            if isinstance(meta_content, tuple):
                meta_content = meta_content[0]
            meta_to_save = _extract_instrument_metadata(meta_content)
            for field in meta_to_save:
                hdf5_imm.attrs[field] = meta_to_save[field]
        # Il metadata 320 setta i colori. E` importante che i suoi valori siano
        # salvati come unsigned e che abbiano il giusto numero di bit, quindi
        # questo caso merita di essere trattato singolarmente
        elif key == 320:
            hdf5_imm.attrs[key_dict[key]]= numpy.array(metadata[key],dtype=numpy.uint32)
        # Tutti gli metadata vengono salvati semplicemente; se il key e` nel dizionario
        # il metadato viene salvato con il suo nome, altrimenti solo con il suo numero
        elif key in key_dict:
            hdf5_imm.attrs[key_dict[key]]= metadata_convert_hdf5(metadata[key])
        else:
            hdf5_imm.attrs['TIFF_METADATA_'+str(key)] = metadata_convert_hdf5(metadata[key])

    # Aggiungo i metadata che riportano l'ora e il nome dello script che ha generato il file
    hdf5_imm.attrs['HDF5 generated by'] = numpy.string_('SEM2hdf5 script')
    now = time.gmtime()
    date = str(now[0]) + '/' + str(now[1]).zfill(2) + '/' + str(now[2]).zfill(2)
    hhmmss = str(now[3]).zfill(2) + ":" + str(now[4]).zfill(2) + ':' + str(now[5]).zfill(2)
    hdf5_imm.attrs['HDF5 generated time'] = numpy.string_(date + ' at ' + hhmmss + ' UTC')
    # Aggiungo i metadati opzionali (esempio: label dell'image recognition)
    if additional_metadata is not None:
        for field in additional_metadata:
            value = numpy.string_(additional_metadata[field])
            hdf5_imm.attrs[field] = value

    f.close()

def get_image_metadata(image_path):
    __, metadata = open_image(image_path)
    key_list = sorted(metadata)


    try:
        instrument_metadata = metadata.pop(34118)
        key_list.remove(34118)
    except:
        pass

    # Now I remove the palete metadata, which is useless and quite big
    try:
        instrument_metadata = metadata.pop(320)
        key_list.remove(320)
    except:
        pass

    # Converting metadata in a JSON friendly format adding a human readable description
    new_dict = dict()
    for key in key_list:
        new_dict[key_dictionary(key,True)] = metadata_convert_json(metadata[key])

    return new_dict

def get_microscope_metadata(image_path):
    # Raises: NoMicroscopeMetadata
    __, metadata = open_image(image_path)

    if not 34118 in metadata:
        raise NoMicroscopeMetadata

    mic_meta = metadata[34118]
   # Now I will put the text inside the TIFF field into a dictionary
    mic_meta = _extract_instrument_metadata(mic_meta, True)
    # and now i will try to convert units od measurement and percentages
    key_list = [key for key in mic_meta]
    for key in key_list:
        field = mic_meta[key]
        changed = False
        new_key = key
        new_field = field

        # Check if there are some data which are a number followed by a unit of measurement
        try:
            _, _, number, unit = read_unit_number(field)
            new_key = key + ' (' + str(unit) + ')'
            new_field = number
            changed = True
        except NoUnitOfMeasurementFound:
            pass

        # Check for integer numbers
        if not changed:
            try:
                new_field = int(field)
                changed= True
            except:
                pass

        # Check for floating numbers
        if not changed:
            try:
                new_field = float(field)
                changed= True
            except:
                pass

        # Check for percentaged
        if not changed:
            field_sp = field.split()
            if ( (len(field_sp) == 2 and field_sp[1]=='%')
                 or (len(field_sp)==1 and field[-1]=='%')):
                new_key = key + (' (%)')
                new_field = new_field[:-1].strip()
                try:
                    new_field = float(new_field)
                    changed=True
                except:
                    pass

        # Now I save the changes
        if changed:
            if key==new_key:
                mic_meta[key] = new_field
            else:
                mic_meta.pop(key)
                mic_meta[new_key] = new_field
    return mic_meta



def get_json_metadata(image_path,
                      do_id,
                      inv_id,
                      study_id,
                      upload_started,
                      upload_ended,
                      file_name=None,
                      image_sha256=None,
                      hdf5_sha256=None):
    output = dict()
    if file_name == None:
        file_name = os.path.basename(image_path)
    output['digital object id'] = do_id
    output['investigation id'] = inv_id
    output['study id'] = study_id
    output['upload_started'] = upload_started
    output['upload_ended'] = upload_ended
    output['file metadata'] = get_image_metadata(image_path)
    output['instrument metadata'] = get_microscope_metadata(image_path)
    output['files name'] = [file_name]
    if image_sha256 != None and image_sha256 != '':
        output['image stored'] = image_sha256
    if hdf5_sha256 != None and hdf5_sha256 != '':
        output['files hash'] = [hdf5_sha256]
    return json.dumps(output,indent=2)



