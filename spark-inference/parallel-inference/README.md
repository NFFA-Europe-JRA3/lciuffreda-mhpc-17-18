Author: Luca Ciuffreda

# Running inference on a Spark cluster

This folder contains files to run a inference test on a Spark Standalone cluster.

## Files and directories

### measure_inference.py

	Main file.
	In order to run it sparkmeasure to be available:
	
	   pip install sparkmeasure

	Or download and compile the package from https://github.com/LucaCanali/sparkMeasure.
	
	Run it as follows:

	     spark-submit --properties-file spark-add.conf measure_inference.py \
		     	  --images-folder ${DATA_DIR} \
		     	  --model-folder ${MODEL_DIR} \
		     	  --convert-tif 'true'/'false'

	Where spark-add.conf is a file for additional Spark configurations (see run_inference.sh/create_spark_conf.py), and convert-tif is an option for converting .tiffs to .jpg. DATA_DIR and MODEL_DIR are directories containg the images and the model checkpoint .pb, respectively.

### create_spark_conf.py

    Script that creates a temporary Spark file 'spark-add.conf'  with extra options to be used with the flag --properties-file (see run_inference.sh).

### run_inference.sh

	Run the measure_inference.py scripts with the parameters specified in params.conf. 

### params.conf

	Definition of the parameters used to run the inference: number of executor per node (EXEC_NODE), number of cores per executor (CORES_EX) and memory per executor (MEM_EX).

### SEM2hdf5.py/units_of_measurement.py

	Dependencies for the main script.

### sparkmeasure_analysis.ipynb

	Template jupyter notebook for analysing the output of sparkmeasure. 

### out

	Output folder. It containes an example of sparkmeasure output (aggr_stagemetrics_12-08-18-15:16:35), which contains a .json file with a summary of the statistics and a conf-report.txt file with some info of the application configuration.



