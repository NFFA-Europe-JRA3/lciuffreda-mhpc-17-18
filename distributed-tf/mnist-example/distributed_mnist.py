from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
 
import sys
import argparse
import math
import os
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
from tensorflow.examples.tutorials.mnist import mnist
 
FLAGS = None
val_batch_size=1000
batch_size = 100
num_epochs=10000

import numpy as np
import struct
import time

INPUT = os.path.join(os.getcwd(),'data')
OUTDIR= os.path.join(os.getcwd(),'out')
if not os.path.exists(INPUT):
    os.makedirs(INPUT)
if not os.path.exists(OUTDIR):
    os.makedirs(OUTDIR)

def decode(serialized_example):
    """
    Parses an image and label from the given `serialized_example`. This function is specific to the MNIST dataset; in other cases it must be adapted.
    """
    features = tf.parse_single_example(
        serialized_example,
        # Defaults are not specified since both keys are required.
        features={
            'image_raw': tf.FixedLenFeature([], tf.string),
            'label': tf.FixedLenFeature([], tf.int64),
        })

    # Convert from a scalar string tensor (whose single string has
    # length mnist.IMAGE_PIXELS) to a uint8 tensor with shape
    # [mnist.IMAGE_PIXELS].
    image = tf.decode_raw(features['image_raw'], tf.uint8)
    image.set_shape((mnist.IMAGE_PIXELS)) # mnist.IMAGE_PIXELS = 784

    # Convert label from a scalar uint8 tensor to an int32 scalar.
    label = tf.cast(features['label'], tf.int32)

    return image, label

def normalize(image, label):
    """
    Convert `image` from [0, 255] -> [-0.5, 0.5] floats.
    """
    image = tf.cast(image, tf.float32) * (1. / 255) - 0.5
    return image, label

def inputs(train, batch_size, num_epochs):
    """Reads input data num_epochs times.
    
    Args:
    train: Selects between the training (True) and validation (False) data.
    batch_size: Number of examples per returned batch.
    num_epochs: Number of times to read the input data, or 0/None to
       train forever.
    
    Returns:
    A tuple (images, labels), where:
    * images is a float tensor with shape [batch_size, mnist.IMAGE_PIXELS]
      in the range [-0.5, 0.5].
    * labels is an int32 tensor with shape [batch_size] with the true label,
      a number in the range [0, mnist.NUM_CLASSES).
    
    This function creates a one_shot_iterator, meaning that it will only iterate
    over the dataset once. On the other hand there is no special initialization
    required.
    """
    if not num_epochs:
        num_epochs = None

    # Definition of input tfRecords.
    filename = [os.path.join(INPUT,'train.tfrecords')]
    if not train:
        filename=[os.path.join(INPUT,'validation.tfrecords')]

    with tf.name_scope('input'):
        # TFRecordDataset opens a binary file and reads one record at a time.
        # `filename` could also be a list of filenames, which will be read in order.
        dataset = tf.data.TFRecordDataset(filename)

        # Decode and transorfm the dataset
        dataset = dataset.map(decode)
        dataset = dataset.map(normalize)
        dataset = dataset.shuffle(1000 + 3 * batch_size)
        dataset = dataset.repeat(num_epochs)
        if not train:
            dataset = dataset.repeat()
        dataset = dataset.batch(batch_size)

        iterator = dataset.make_one_shot_iterator()
        
    return iterator.get_next()

def variable_summaries(var):
    """
    Attach a lot of summaries of the variable 'var' to a Tensor (for TensorBoard visualization).
    """
    with tf.name_scope('summaries'):
        mean = tf.reduce_mean(var)
        tf.summary.scalar('mean', mean)
        with tf.name_scope('stddev'):
            stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
        tf.summary.scalar('stddev', stddev)
        tf.summary.scalar('max', tf.reduce_max(var))
        tf.summary.scalar('min', tf.reduce_min(var))
        tf.summary.histogram('histogram', var)

def compute_accuracy(logits, labels):
    
    predictions = tf.argmax(logits, axis=1, output_type=tf.int64)
    labels = tf.cast(labels, tf.int64)
    
    return tf.reduce_sum(tf.cast(tf.equal(predictions, labels), dtype=tf.float32)) / val_batch_size


####################################################################################################


def main(_):

    NUM_GPUS=2
    GPU=FLAGS.task_index%NUM_GPUS
    worker_device="/job:%s/task:%d/gpu:%d" % (FLAGS.job_name, FLAGS.task_index,GPU)
    print('I am :', worker_device)
    
    with tf.device(tf.train.replica_device_setter(worker_device=worker_device,cluster=cluster)):
 
        ###
        ### Training
        ###

        # Define input batches for training/validation
        val_example, val_label=inputs(train=False, batch_size=val_batch_size, num_epochs=num_epochs) 
        next_example, next_label = inputs(batch_size=batch_size, num_epochs=num_epochs) 
        
        # Define network and inference
        # 2 fully connected hidden layers : 784->128->64->10
        with tf.name_scope('hidden1'):
            weights = tf.Variable(tf.truncated_normal([784, 128],stddev=1.0 / math.sqrt(float(784))),name='weights')
            biases = tf.Variable(tf.zeros([128]),name='biases')
            hidden1 = tf.nn.relu(tf.matmul(next_example, weights) + biases)
        with tf.name_scope('hidden2'):
            weights = tf.Variable(tf.truncated_normal([128, 64],stddev=1.0 / math.sqrt(float(128))),name='weights')
            biases = tf.Variable(tf.zeros([64]),name='biases')
            hidden2 = tf.nn.relu(tf.matmul(hidden1, weights) + biases)
        with tf.name_scope('softmax_linear'):
            weights = tf.Variable(tf.truncated_normal([64, 10],stddev=1.0 / math.sqrt(float(64))),name='weights')
            biases = tf.Variable(tf.zeros([10]),name='biases')
            logits = tf.matmul(hidden2, weights) + biases

        # Define optimization
        global_step = tf.train.create_global_step()
        learning_rate=FLAGS.lr*math.sqrt(len(FLAGS.worker_hosts.split(",")))*0.07
        optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate)
        loss = tf.losses.sparse_softmax_cross_entropy(labels=next_label,logits=logits)
        tf.summary.scalar('loss', loss)       
        train_op = optimizer.minimize(loss=loss,global_step=global_step)

        # Validation on CPU
        with tf.device('/job:ps/task:0/cpu:0'):
            val_hidden1 = tf.nn.relu(tf.matmul(val_example, h1_weights) + h1_biases)
            val_hidden2 = tf.nn.relu(tf.matmul(val_hidden1, h2_weights) + h2_biases)
            val_logits=tf.matmul(val_hidden2, softmax_weights) + softmax_biases
            accuracy, acc_op = tf.metrics.accuracy(labels=val_label, predictions=tf.argmax(val_logits,1))
        tf.summary.scalar('accuracy', accuracy)

    # Set up and run session
    hooks=[tf.train.StopAtStepHook(last_step=num_epochs)]
   
    config = tf.ConfigProto(allow_soft_placement=True,log_device_placement=True)
    config.gpu_options.allow_growth = True

    with tf.train.MonitoredTrainingSession(master=server.target,checkpoint_dir=OUTDIR,is_chief=is_chief,config=config,hooks=hooks) as sess:
        local_step_value = 0
        while not sess.should_stop():
            image_val,_, global_step_value, loss_value, acc_op_value, accuracy_value = sess.run([next_example,train_op, global_step, loss, acc_op, accuracy])
            local_step_value += 1
            if local_step_value % 100 == 0: 
                print("Local Step %d, Global Step %d (Loss: %.2f), Accuracy: %.2f %.2f" % (local_step_value, global_step_value, loss_value, accuracy_value, acc_op_value)) 
        print('training finished')
             
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--train_file',
        type=str,
        default=INPUT,
        help='File path for the training data.')
    parser.add_argument(
        '--out_dir',
        type=str,
        default='/home/demouser/out',
        help='Dir path for the model and checkpoint output.')
    parser.add_argument(
        '--job_name',
        type=str,
        required=True,
        help='job name (parameter or worker) for cluster')
    parser.add_argument(
        '--task_index',
        type=int,
        required=True,
        help='index number in job for cluster')
    parser.add_argument(
        "--ps_hosts",
        type=str,
        default="",
        help="Comma-separated list of hostname:port pairs"
    )
    parser.add_argument(
        "--worker_hosts",
        type=str,
        default="",
        help="Comma-separated list of hostname:port pairs"
    )
    
    FLAGS, unparsed = parser.parse_known_args()
 
    # Start server
    ps_hosts = FLAGS.ps_hosts.split(",")
    worker_hosts = FLAGS.worker_hosts.split(",")
    cluster = tf.train.ClusterSpec({"ps": ps_hosts, "worker": worker_hosts})
    server = tf.train.Server(cluster,job_name=FLAGS.job_name,task_index=FLAGS.task_index)
    
    if FLAGS.job_name == "ps":
        server.join()
    elif FLAGS.job_name == "worker":
        is_chief = (FLAGS.task_index == 0)
        tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)
