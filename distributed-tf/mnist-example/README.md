Author: Luca Ciuffreda

# Distributed TF example

This folder contains the implementation of a 2-layer classifier for the MNIST dataset with Distributed TensorFlow.

The main code **distributed_mnist.py** starts a training session over a cluster of hosts, each equipped with two GPUs. To start the session on a ps server run

    CUDA_VISIBLE_DEVICES= python distributed_mnist.py --ps_hosts <PS_IP>:2222 --worker_hosts <PS_IP>:2223,<WORKER_0_IP>:2222,...,<WORKER_N_IP>:2222 --job_name ps --task_index 0

while on a worker

    python distributed_mnist.py --ps_hosts <PS_IP>:2222 --worker_hosts <PS_IP>:2223,<WORKER_0_IP>:2222,...,<WORKER_N_IP>:2222 --job_name worker --task_index <0 ... N>

## Remarks:

	* On the ps use CUDA_VISIBLE_DEVICES= to hide the GPUs form the ps server. This allows other worker processes to start without causing memory issues.
	* To run properly with two GPUs per node the task indices must be in the following way: consider a 2 node cluster, for instance. On node 0 (say its also the ps),
	
	     python distributed_mnist.py --ps_hosts <NODE_0_IP>:2222 --worker_hosts <NODE_0_IP>:2223,<NODE_0_IP>:2224,<NODE_1_IP>:2222,<NODE_1_IP>:2223 --job_name worker --task_index 0 
	     python distributed_mnist.py --ps_hosts <NODE_0_IP>:2222 --worker_hosts <NODE_0_IP>:2223,<NODE_0_IP>:2224,<NODE_1_IP>:2222,<NODE_1_IP>:2223 --job_name worker --task_index 1 

	  will start the workers on GPU 0 and 1. On node-1, instead run

	     python distributed_mnist.py --ps_hosts <NODE_0_IP>:2222 --worker_hosts <NODE_0_IP>:2223,<NODE_0_IP>:2224,<NODE_1_IP>:2222,<NODE_1_IP>:2223 --job_name worker --task_index 2 
	     python distributed_mnist.py --ps_hosts <NODE_0_IP>:2222 --worker_hosts <NODE_0_IP>:2223,<NODE_0_IP>:2224,<NODE_1_IP>:2222,<NODE_1_IP>:2223 --job_name worker --task_index 3

	  Task index is mapped to #GPU=task_index%2, in this case again 0,1.
	 * When running multiple services on the same machine start different independent shells. Also, change the port associated to the IP: e.g. to start a worker on the ps server, change the port to 2223 in the worker_list

## Files and directories

   * **distributed_mnist.py** : main file. It outputs a folder 'out' with checkpoints ans summaries to be opened wit TensorBoard
     			      	tensorboard --logdir out
   * **data** : folder with input tfRecords
   * **results** : folder with output results for a scaling test N_gpus=1->6